#pragma once

#include "ray.hpp"
#include "vec3.hpp"

namespace gpu {

template <typename Float>
struct AABB {
    vec3<Float> rMin;
    vec3<Float> rMax;

    AABB operator+(const AABB& box) const noexcept {
        return {.rMin = min(rMin, box.rMin), .rMax = max(rMax, box.rMax)};
    }
};

template <typename Float>
[[nodiscard]] RayRange<Float> intersection(const Ray<Float>& ray, const RayRange<Float>& range,
                                           const AABB<Float>& box) noexcept {
    const vec3<Float> v1   = (box.rMin - ray.origin) / ray.direction;
    const vec3<Float> v2   = (box.rMax - ray.origin) / ray.direction;
    const vec3<Float> tMin = min(v1, v2);
    const vec3<Float> tMax = max(v1, v2);

    return {.tMin = maxf(tMin.x, maxf(tMin.y, maxf(tMin.z, range.tMin))),
            .tMax = minf(tMax.x, minf(tMax.y, minf(tMax.z, range.tMax)))};
}

}  // namespace gpu
