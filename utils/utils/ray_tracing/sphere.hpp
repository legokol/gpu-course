#pragma once

#include "ray.hpp"

namespace gpu {

template <typename Float>
struct Sphere {
    vec3<Float> origin;
    Float radius;
};

template <typename Float>
[[nodiscard]] RayRange<Float> intersection(const Ray<Float> &ray, [[maybe_unused]] const RayRange<Float> &range,
                                           const Sphere<Float> &sphere) noexcept {
    const vec3 v        = ray.origin - sphere.origin;
    const Float dv      = dot(ray.direction, v);
    const Float d2      = squaredNorm(ray.direction);
    const Float det     = dv * dv - d2 * (squaredNorm(v) - sphere.radius * sphere.radius);
    const Float detSqrt = sqrt(det);
    const RayRange r{.tMin = (-dv - detSqrt) / d2, .tMax = (-dv + detSqrt) / d2};
    return r;
//    return nonempty(range + r) ? std::optional(range + r) : std::nullopt;
}

}  // namespace gpu
