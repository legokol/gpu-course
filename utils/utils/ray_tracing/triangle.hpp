#pragma once

#include "ray.hpp"

namespace gpu {

template <typename Float>
struct Triangle {
    vec3<Float> r0;
    vec3<Float> r1;
    vec3<Float> r2;
};

template <typename Float>
struct RayTriangleIntersection {
    Float t;
    Float b1;
    Float b2;
    bool front;
};

template <typename Float, typename T>
[[nodiscard]] auto interpolate(const T &r, Float p, Float q) {
    return r[0] * (1.f - p - q) + r[1] * p + r[2] * q;
}

template <typename Float>
[[nodiscard]] bool hit(const RayTriangleIntersection<Float> &i) noexcept {
    return (i.b1 >= 0.f) && (i.b2 >= 0.f) && (1.f - i.b1 - i.b2 >= 0.f);
}

template <typename Float>
[[nodiscard]] Float hitDistance(const RayTriangleIntersection<Float> &i) noexcept {
    return i.t;
}

template <typename Float>
[[nodiscard]] RayTriangleIntersection<Float> intersection(const Ray<Float> &ray, const RayRange<Float> &range,
                                                          const Triangle<Float> &t) noexcept {
    const vec3 e1 = t.r1 - t.r0;
    const vec3 e2 = t.r2 - t.r0;
    const vec3 v  = ray.origin - t.r0;
    const vec3 d  = -ray.direction;

    const Float det0 = dot(d, cross(e1, e2));

    const RayTriangleIntersection i{.t     = dot(v, cross(e1, e2)) / det0,
                                    .b1    = dot(d, cross(v, e2)) / det0,
                                    .b2    = dot(d, cross(e1, v)) / det0,
                                    .front = det0 > 0.f};
    return contains(range, i.t) && hit(i) ? i
                                          : RayTriangleIntersection{.t = 0.f, .b1 = -1.f, .b2 = -.1f, .front = false};
}

}  // namespace gpu
