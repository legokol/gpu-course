#pragma once

#include "vec3.hpp"

namespace gpu {

template <typename Float>
struct Ray {
    vec3<Float> origin;
    vec3<Float> direction;
};

template <typename Float>
struct RayRange {
    Float tMin;
    Float tMax;

    static RayRange ZeroToInf() noexcept { return {.tMin = 0.f, .tMax = 1.f / 0.f}; }
};

template <typename Float>
[[nodiscard]] RayRange<Float> operator+(const RayRange<Float> &l, const RayRange<Float> &r) noexcept {
    return {maxf(l.tMin, r.tMin), minf(l.tMax, r.tMax)};
}

template <typename Float>
[[nodiscard]] auto contains(const RayRange<Float> &r, Float t) noexcept {
    return r.tMin <= t && t <= r.tMax;
}
template <typename Float>
[[nodiscard]] auto nonempty(const RayRange<Float> &r) noexcept {
    return r.tMin <= r.tMax;
}
template <typename Float>
[[nodiscard]] auto hit(const RayRange<Float> &r) noexcept {
    return r.tMin <= r.tMax;
}
template <typename Float>
[[nodiscard]] auto empty(const RayRange<Float> &r) noexcept {
    return !nonempty(r);
}

template <typename Float>
[[nodiscard]] Float hitDistance(const RayRange<Float> &r) noexcept {
    return r.tMin;
}

}  // namespace gpu
