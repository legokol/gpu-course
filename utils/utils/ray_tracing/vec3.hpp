#pragma once

#include <cmath>
#include <cstddef>
#include <experimental/simd>

namespace gpu {

template <typename T>
struct vec3 {
    T x;
    T y;
    T z;

    T& operator[](std::size_t i) noexcept { return (&x)[i]; }
    const T& operator[](std::size_t i) const noexcept { return (&x)[i]; }

    vec3 operator-() const noexcept { return {-x, -y, -z}; }
};

using ABI    = std::experimental::simd_abi::fixed_size<4>;
using f32x4  = std::experimental::simd<float, ABI>;
using vec3x4 = vec3<f32x4>;

[[nodiscard]] inline f32x4 buildSimdFloat(const std::array<float, 4>& f) noexcept {
    f32x4 res;
    for (unsigned i = 0; i < f32x4::size(); ++i) {
        res[i] = f[i];
    }
    return res;
}

[[nodiscard]] inline std::array<vec3<float>, 4> fromSimdVec(const vec3x4& v) noexcept {
    std::array<vec3<float>, 4> res{};
    for (unsigned i = 0; i < f32x4::size(); ++i) {
        res[i] = {v.x[i], v.y[i], v.z[i]};
    }
    return res;
}

[[nodiscard]] inline vec3x4 buildSimdVec(const std::array<vec3<float>, 4>& v) noexcept {
    vec3x4 res;
    for (unsigned i = 0; i < f32x4::size(); ++i) {
        res.x[i] = v[i].x;
        res.y[i] = v[i].y;
        res.z[i] = v[i].z;
    }
    return res;
}

[[nodiscard]] inline float minf(float x, float y) { return x < y ? x : y; }
[[nodiscard]] inline float maxf(float x, float y) { return x > y ? x : y; }

[[nodiscard]] inline f32x4 minf(const f32x4& x, f32x4 y) {
    where(x < y, y) = x;
    return y;
}
[[nodiscard]] inline f32x4 maxf(const f32x4& x, f32x4 y) {
    where(x > y, y) = x;
    return y;
}

[[nodiscard]] inline float minf(const f32x4::mask_type& m, const f32x4& f) {
    float res = 1.0f / 0.0f;
    for (unsigned i = 0; i < f32x4::size(); ++i) {
        if (m[i]) {
            res = minf(res, f[i]);
        }
    }
    return res;
}

[[nodiscard]] inline float sqrt(float x) { return std::sqrt(x); }

[[nodiscard]] inline f32x4 sqrt(f32x4 x) {
    for (unsigned i = 0; i < f32x4::size(); ++i) {
        x[i] = std::sqrt(x[i]);
    }
    return x;
}

template <typename T>
[[nodiscard]] vec3<T> operator+(const vec3<T>& l, const vec3<T>& r) noexcept {
    return {l.x + r.x, l.y + r.y, l.z + r.z};
}
template <typename T>
[[nodiscard]] vec3<T> operator-(const vec3<T>& l, const vec3<T>& r) noexcept {
    return {l.x - r.x, l.y - r.y, l.z - r.z};
}

template <typename T>
[[nodiscard]] vec3<T> operator*(const vec3<T>& l, const vec3<T>& r) noexcept {
    return {l.x * r.x, l.y * r.y, l.z * r.z};
}
template <typename T>
[[nodiscard]] vec3<T> operator/(const vec3<T>& l, const vec3<T>& r) noexcept {
    return {l.x / r.x, l.y / r.y, l.z / r.z};
}

template <typename T>
[[nodiscard]] vec3<T> operator*(const vec3<T>& v, T f) noexcept {
    return {v.x * f, v.y * f, v.z * f};
}
template <typename T>
[[nodiscard]] vec3<T> operator*(T f, const vec3<T>& v) noexcept {
    return {v.x * f, v.y * f, v.z * f};
}
template <typename T>
[[nodiscard]] vec3<T> operator/(const vec3<T>& v, T f) noexcept {
    return {v.x / f, v.y / f, v.z / f};
}

template <typename T>
[[nodiscard]] T dot(const vec3<T>& v1, const vec3<T>& v2) noexcept {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

template <typename T>
[[nodiscard]] vec3<T> cross(const vec3<T>& v1, const vec3<T>& v2) noexcept {
    return vec3<T>{.x = v1.y * v2.z - v1.z * v2.y, .y = v1.z * v2.x - v1.x * v2.z, .z = v1.x * v2.y - v1.y * v2.x};
}

template <typename T>
[[nodiscard]] vec3<T> min(const vec3<T>& v1, const vec3<T>& v2) {
    return {minf(v1.x, v2.x), minf(v1.y, v2.y), minf(v1.z, v2.z)};
}

template <typename T>
[[nodiscard]] vec3<T> max(const vec3<T>& v1, const vec3<T>& v2) {
    return {maxf(v1.x, v2.x), maxf(v1.y, v2.y), maxf(v1.z, v2.z)};
}

[[nodiscard]] float norm(const vec3<float>& v) { return std::hypot(v.x, v.y, v.z); }
[[nodiscard]] f32x4 norm(const vec3<f32x4>& v) {
    f32x4 res;
    for (unsigned i = 0; i < f32x4::size(); ++i) {
        res[i] = std::hypot(v.x[i], v.y[i], v.z[i]);
    }
    return res;
}

template <typename T>
[[nodiscard]] T squaredNorm(const vec3<T>& v) noexcept {
    return dot(v, v);
}

template <typename T>
[[nodiscard]] vec3<T> normalize(const vec3<T>& v) {
    return v / norm(v);
}

}  // namespace gpu
