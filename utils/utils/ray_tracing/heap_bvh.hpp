#pragma once

#include <algorithm>
#include <bit>
#include <concepts>
#include <limits>
#include <queue>
#include <ranges>
#include <vector>

#ifndef STACKLESS
#include <stack>
#endif

#include "aabb.hpp"
#include "ray.hpp"

namespace gpu {

template <typename Float, typename T>
struct HeapBVH {
    std::vector<unsigned> order;

    std::vector<AABB<Float>> boxes;
    std::vector<T> geometry;
};

namespace details {

template <typename Float>
using IBox = std::pair<unsigned, AABB<Float>>;

template <typename Float>
std::pair<std::vector<unsigned>, std::vector<AABB<Float>>> createHeapBVHImpl(const std::span<IBox<Float> const> ibox) {
    const unsigned leafCount = unsigned(ibox.size());

    std::vector<unsigned> order(leafCount);
    std::ranges::iota(order, 0u);
    std::vector<AABB<Float>> boxes(2u * leafCount - 1u);

    std::queue<std::pair<unsigned, unsigned>> range;
    range.emplace(0, leafCount);

    for (unsigned i = 0; i + 1u < leafCount; ++i) {
        const auto [b, e] = range.front();
        range.pop();
        const unsigned n = e - b;
        const auto ord   = order | std::views::drop(b) | std::views::take(n);

        const AABB commonBox = *std::ranges::fold_left_first(
            ord | std::views::transform([&ibox](unsigned k) { return ibox[k].second; }), std::plus{});
        boxes[i]               = commonBox;
        const vec3<Float> diag = commonBox.rMax - commonBox.rMin;

        const auto proj = [&ibox, i = diag.x > diag.y ? (diag.x > diag.z ? 0u : 2u)
                                                      : (diag.y > diag.z ? 1u : 2u)](unsigned k) {
            const auto &[rMin, rMax] = ibox[k].second;
            return rMin[i] + rMax[i];
        };

        const unsigned left  = 1u << unsigned(std::log2(n - 1u));
        const unsigned right = left >> 1u;
        const unsigned mid   = n < left + right ? n - right : left;

        std::ranges::nth_element(ord, std::ranges::begin(ord) + mid, std::less{}, proj);

        range.emplace(b, b + mid);
        range.emplace(b + mid, e);
    }
    const unsigned left = 1u << unsigned(std::log2(leafCount - 1u));
    std::ranges::rotate(order, order.begin() + 2 * (leafCount - left));

    for (unsigned i = leafCount - 1u; i < 2u * leafCount - 1u; ++i) {
        boxes[i] = ibox[order[i - (leafCount - 1u)]].second;
    }

    return {order, boxes};
}

}  // namespace details

template <typename Float, std::ranges::random_access_range Boxable, typename F>
[[nodiscard]] auto createHeapBVH(Boxable &&r, const F &toBox) -> HeapBVH<Float, std::ranges::range_value_t<Boxable>> {
    const auto boxER =
        r | std::views::enumerate | std::views::transform([&toBox](const auto &pair) -> details::IBox<Float> {
            const auto &[i, obj] = pair;
            return {unsigned(i), toBox(obj)};
        });

    std::vector<details::IBox<Float>> ibox(std::ranges::begin(boxER), std::ranges::end(boxER));
    const auto &[order, boxes] = details::createHeapBVHImpl<float>(ibox);
    return {.order = order, .boxes = boxes, .geometry = std::vector(std::ranges::begin(r), std::ranges::end(r))};
}

template <typename T, typename I>
[[nodiscard]] auto hitDistance(const std::pair<const T *, I> &intersection) {
    return hitDistance(intersection.second);
}

#ifdef STACKLESS

struct TraverseState {
    unsigned count;
    unsigned node;
    unsigned trail;
};

namespace details {

inline TraverseState initTraverse(unsigned leafCount) { return {.count = leafCount, .node = 1u, .trail = 0u}; }

inline bool incomplete(TraverseState state) { return state.node != 0u; }

inline unsigned sibling(unsigned node) { return node == 0u ? 0u : node + 1u - ((node & 1u) << 1u); }

inline TraverseState up(TraverseState state) {
    const unsigned trail = state.trail + 1u;
    const auto up        = static_cast<unsigned>(std::countr_zero(trail));
    return {.count = state.count, .node = sibling(state.node >> up), .trail = trail >> up};
}

inline TraverseState proceed(TraverseState state, bool goLeft, bool goRight) {
    if (goLeft || goRight) {
        const unsigned node = goLeft ? state.node * 2u : state.node * 2u + 1;
        return {.count = state.count, .node = node, .trail = 2u * state.trail + (goLeft xor goRight ? 1u : 0u)};
    }
    return up(state);
}

}  // namespace details

template <typename T>
auto intersection(const Ray<float> &ray, RayRange<float> range, const HeapBVH<float, T> &bvh)
    -> std::pair<const T *, decltype(intersection(ray, range, std::declval<T>()))> {
    using result_t = std::pair<const T *, decltype(intersection(ray, range, std::declval<T>()))>;

    if (!hit(intersection(ray, range, bvh.boxes[0]))) {
        return {nullptr, {}};
    }

    result_t result          = {nullptr, {}};
    float distance           = 1.f / 0.f;
    const unsigned leafCount = unsigned(bvh.geometry.size());

    TraverseState state = details::initTraverse(leafCount);

    while (details::incomplete(state)) {
        const unsigned i = state.node - 1;

        if (i + 1u < leafCount) {
            const auto leftIntersection  = intersection(ray, range, bvh.boxes[2u * i + 1u]);
            const auto rightIntersection = intersection(ray, range, bvh.boxes[2u * i + 2u]);

            const bool goLeft  = hit(leftIntersection) && hitDistance(leftIntersection) < distance;
            const bool goRight = hit(rightIntersection) && hitDistance(rightIntersection) < distance;

            state = details::proceed(state, goLeft, goRight);
        } else {
            const unsigned leafIndex     = i - (leafCount - 1u);
            const unsigned geometryIndex = bvh.order[leafIndex];
            const auto intersect         = intersection(ray, range, bvh.geometry[geometryIndex]);
            if (hit(intersect)) {
                const float dist = hitDistance(intersect);
                if (dist < distance) {
                    distance   = dist;
                    range.tMax = distance;
                    result     = {&bvh.geometry[geometryIndex], intersect};
                }
            }
            state = details::up(state);
        }
    }

    return result;
}

#else

template <typename Float, typename T>
auto intersection(const Ray<Float> &ray, RayRange<Float> range, const HeapBVH<Float, T> &bvh)
    -> std::pair<const T *, decltype(intersection(ray, range, std::declval<T>()))> {
    using result_t = std::pair<const T *, decltype(intersection(ray, range, std::declval<T>()))>;

    if (!hit(intersection(ray, range, bvh.boxes[0]))) {
        return {nullptr, {}};
    }

    result_t result          = {nullptr, {}};
    Float distance           = 1.f / 0.f;
    const unsigned leafCount = unsigned(bvh.geometry.size());
    std::stack<unsigned> s;
    s.push(0);
    while (!s.empty()) {
        const unsigned i = s.top();
        s.pop();

        if (i + 1u < leafCount) {
            const auto leftIntersection  = intersection(ray, range, bvh.boxes[2u * i + 1u]);
            const auto rightIntersection = intersection(ray, range, bvh.boxes[2u * i + 2u]);
            if (hit(leftIntersection) && hitDistance(leftIntersection) < distance) {
                s.push(2u * i + 1u);
            }
            if (hit(rightIntersection) && hitDistance(rightIntersection) < distance) {
                s.push(2u * i + 2u);
            }
        } else {
            const unsigned leafIndex     = i - (leafCount - 1u);
            const unsigned geometryIndex = bvh.order[leafIndex];
            const auto intersect         = intersection(ray, range, bvh.geometry[geometryIndex]);
            if (hit(intersect)) {
                const Float dist = hitDistance(intersect);
                if (dist < distance) {
                    distance   = dist;
                    range.tMax = distance;
                    result     = {&bvh.geometry[geometryIndex], intersect};
                }
            }
        }
    }

    return result;
}
#endif

}  // namespace gpu
