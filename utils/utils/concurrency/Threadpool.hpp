#pragma once

#include <functional>
#include <future>
#include <queue>
#include <thread>
#include <vector>

namespace gpu {

using Task = std::move_only_function<bool()>;

template <typename F, typename... Args>
auto createTask(F f, Args &&...args) {
    std::packaged_task<std::remove_pointer_t<F>> task{f};
    auto future = task.get_future();
    Task t{[ct = std::move(task), ... args = std::forward<Args>(args)] mutable {
        std::invoke(ct, std::forward<Args>(args)...);
        return true;
    }};
    return std::make_pair(std::move(t), std::move(future));
}

class Threadpool {
private:
    std::vector<std::thread> threads_;
    std::queue<Task> tasks_;
    std::size_t inProgress_{0};
    mutable std::mutex mtx_;
    std::condition_variable tasksCond_;
    std::condition_variable waitCond_;

    void run();

    void push(Task t) {
        {
            std::lock_guard lk{mtx_};
            tasks_.push(std::move(t));
        }
        tasksCond_.notify_one();
    }

public:
    explicit Threadpool(std::size_t threadCount) {
        for (std::size_t i = 0; i < threadCount; ++i) {
            threads_.emplace_back(&Threadpool::run, this);
        }
    }

    ~Threadpool() {
        sentinel();
        for (auto &t : threads_) {
            t.join();
        }
    }

    [[nodiscard]] std::size_t size() const noexcept { return threads_.size(); }

    void wait();

    void sentinel() {
        push([] { return false; });
    }

    template <typename F, typename... Args>
    std::future<std::invoke_result_t<F, Args...>> createTask(F f, Args &&...args);
};

void Threadpool::run() {
    bool work = true;
    while (work) {
        std::unique_lock lk{mtx_};
        tasksCond_.wait(lk, [this] { return !tasks_.empty(); });
        Task t = std::move(tasks_.front());
        tasks_.pop();
        ++inProgress_;
        lk.unlock();

        work = t();
        lk.lock();
        --inProgress_;
        lk.unlock();

        waitCond_.notify_one();
    }
    sentinel();
}

void Threadpool::wait() {
    std::unique_lock lk{mtx_};
    waitCond_.wait(lk, [this] { return inProgress_ == 0 && tasks_.empty(); });
}

template <typename F, typename... Args>
std::future<std::invoke_result_t<F, Args...>> Threadpool::createTask(F f, Args &&...args) {
    auto [task, future] = gpu::createTask(f, std::forward<Args>(args)...);
    push(std::move(task));
    return std::move(future);
}

}  // namespace gpu