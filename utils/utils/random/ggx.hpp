#pragma once

#include "fresnel.hpp"
#include "sample.hpp"

namespace gpu::random {

struct GGXDistribution {
    vec2 a;
    mat3 TBN;

    f32 lambda(vec3 const w) const noexcept {
        auto const [x, y, z] = transpose(TBN) * w;
        f32 const a2         = (x * x * (a.x * a.x) + y * y * (a.y * a.y)) / (z * z);
        return 0.5f * (-1.f + std::sqrt(1.f + a2));
    }
    f32 G1(vec3 const w, vec3 const wm) const noexcept { return dot(w, wm) > 0.f ? 1.f / (1.f + lambda(w)) : 0.f; }
    f32 G2Refl(vec3 const wi, vec3 const wh, vec3 const wo) const noexcept {
        return dot(wo, wh) > 0.f && dot(wi, wh) > 0.f ? 1.f / (1.f + lambda(wi) + lambda(wo)) : 0.f;
    }
    f32 G2Refr(vec3 const wi, vec3 const wt, vec3 const wo) const noexcept {
        return dot(wo, wt) * dot(wi, wt) < 0.f ? 1.f / (1.f + lambda(wi) + lambda(wo)) : 0.f;
    }
    f32 D(vec3 const wm) const noexcept {
        vec3 const n = transpose(TBN) * wm;
        vec3 const e = {1.f / a, 1.f};
        return dot(wm, TBN.z) > 0.f
                   ? 1.f / (std::numbers::pi_v<f32> * a.x * a.y * dot(e * e, n * n) * dot(e * e, n * n))
                   : 0.f;
    }
};

struct GGXVisibleMicrofacetSampler {
    GGXDistribution ggx;

    vec3 sample(vec3 const w) const noexcept {
        vec3 const W = transpose(ggx.TBN) * w;

        // Section 3.2: transforming the view direction to the hemisphere configuration
        vec3 const Vh = normalize(W * vec3(ggx.a, W.z > 0.f ? 1.f : -1.f));

        // Section 4.1: orthonormal basis (with special case if cross product is zero)
        vec3 const T  = cross(vec3(0.f, 0.f, 1.f), Vh);
        vec3 const T1 = dot(T, T) > 1e-8f ? normalize(T) : vec3(1.f, 0.f, 0.f);
        vec3 const T2 = cross(Vh, T1);

        // Section 4.2: parameterization of the projected area
        auto const [cx, cy] = uniformCircleSample();
        f32 const t1        = cx;
        f32 const t2        = std::lerp(std::sqrt(1.f - cx * cx), cy, 0.5f * (1.f + Vh.z));

        // Section 4.3: reprojection onto hemisphere
        vec3 const Nh = t1 * T1 + t2 * T2 + std::sqrt(std::max(0.f, 1.f - t1 * t1 - t2 * t2)) * Vh;

        // Section 3.4: transforming the normal back to the ellipsoid configuration
        return ggx.TBN * normalize(Nh * vec3(ggx.a, 1.f));
    }
    f32 pdfW(vec3 const w, vec3 const ws) const noexcept  // PDF per projected (!) solid angle
    {
        return dot(w, ws) > 0.f && dot(w, ggx.TBN.z) != 0.f
                   ? ggx.G1(w, ws) * ggx.D(ws) * dot(w, ws) / (dot(w, ggx.TBN.z) * dot(w, ggx.TBN.z))
                   : 0.f;
    }
};

struct GGXVisibleReflectionSampler {
    GGXVisibleMicrofacetSampler microfacet;

    vec3 sample(vec3 const w) const noexcept {
        vec3 const wm = microfacet.sample(w);
        return dot(w, microfacet.ggx.TBN.z) > 0.f ? reflect(w, wm) : reflect(w, -wm);
    }
    f32 pdfW(vec3 const w, vec3 const ws) const noexcept {
        vec3 const wh = reflectHalfway(w, microfacet.ggx.TBN.z, ws);
        return microfacet.pdfW(w, wh) * reflectJacobian(w, wh);
    }
};

struct GGXVisibleRefractionSampler {
    f32 n;
    GGXVisibleMicrofacetSampler microfacet;

    vec3 sample(vec3 const w) const noexcept {
        vec3 const wm = microfacet.sample(w);
        return refract(w, wm, n);
    }
    f32 pdfW(vec3 const w, vec3 const ws) const noexcept {
        vec3 const norm = microfacet.ggx.TBN.z;
        vec3 const wt   = refractHalfway(w, norm, ws, n);
        return microfacet.pdfW(dot(w, norm) > 0.f ? w : -reflect(w, norm), wt) * refractJacobian(w, norm, ws, n);
    }
};

}  // namespace gpu::random
