#pragma once

#include <array>

#include "types.hpp"

namespace gpu::random {

inline std::array<c32, 4> completeFresnel(vec3 const wi, vec3 const norm, c32 const n) noexcept  // n = nt / ni
{
    if (dot(norm, wi) < 0.f) return completeFresnel(wi, -norm, 1.f / n);

    f32 const cosThetaI  = dot(norm, wi);
    f32 const sin2ThetaI = std::max(0.f, 1.f - cosThetaI * cosThetaI);

    c32 const sin2ThetaT = sin2ThetaI / (n * n);
    c32 const cosThetaT  = std::sqrt(c32(1.f, 0.f) - sin2ThetaT);

    c32 const rs = (cosThetaI - n * cosThetaT) / (cosThetaI + n * cosThetaT);
    c32 const rp = (n * cosThetaI - cosThetaT) / (n * cosThetaI + cosThetaT);
    return std::array{
        rs,
        c32(1.f, 0.f) + rs,
        rp,
        (c32(1.f, 0.f) + rp) / n,
    };
}
inline f32 fresnelReflectance(vec3 const wi, vec3 const norm, c32 const n) noexcept {
    auto const [rs, ts, rp, tp] = completeFresnel(wi, norm, n);
    return 0.5f * (std::norm(rs) + std::norm(rp));
}
inline vec3 goldFresnelReflectance(vec3 const wi, vec3 const norm) noexcept {
    c32 const nGold471 = c32(1.31f, -1.849f);
    c32 const nGold520 = c32(0.62f, -2.081f);
    c32 const nGold617 = c32(0.21f, -3.272f);

    return {
        fresnelReflectance(wi, norm, nGold617),
        fresnelReflectance(wi, norm, nGold520),
        fresnelReflectance(wi, norm, nGold471),
    };
}

inline c32 iorFrom(f32 const F0) noexcept { return {(1.f + std::sqrt(F0)) / (1.f - std::sqrt(F0)), 0.f}; }
inline vec3 fresnelReflectanceF0(vec3 const wi, vec3 const norm, vec3 const F0) noexcept {
    return {
        fresnelReflectance(wi, norm, iorFrom(F0.x)),
        fresnelReflectance(wi, norm, iorFrom(F0.y)),
        fresnelReflectance(wi, norm, iorFrom(F0.z)),
    };
}

inline vec3 reflect(vec3 const wi, vec3 const norm) noexcept { return -wi + 2.f * norm * dot(wi, norm); }
inline f32 reflectJacobian(vec3 const wi, vec3 const norm) noexcept { return 0.25f / gltf::abs(dot(wi, norm)); }
inline vec3 reflectHalfway(vec3 const wi, vec3 const norm, vec3 const wo) noexcept {
    vec3 const wh = normalize(wi + wo);
    return dot(wh, norm) > 0.f ? wh : -wh;
}

// norm always points into the air, n_air = 1;
// in case dot(wi, norm) > 0, ni = 1, no = n, no / ni =     n
//                 otherwise, ni = n, no = 1, no / ni = 1 / n
inline vec3 refract(vec3 const wi, vec3 const norm, f32 const n) noexcept {
    if (dot(wi, norm) < 0.f) return refract(wi, -norm, 1.f / n);

    f32 const cosThetaT2 = std::max(0.f, 1.f - (1.f - dot(wi, norm) * dot(wi, norm)) / (n * n));
    return -norm * std::sqrt(cosThetaT2) - (wi - norm * dot(wi, norm)) / n;
}
inline vec3 refractHalfway(vec3 const wi, vec3 const norm, vec3 const wo, f32 const n) noexcept {
    vec3 const wt = dot(wi, norm) < 0.f ? normalize(n * wi + wo) : normalize(wi + n * wo);
    return n > 1.f ? -wt : wt;
}
inline f32 refractJacobian(vec3 const wi, vec3 const norm, vec3 const wo, f32 const n) noexcept {
    if (dot(wi, norm) < 0.f) return refractJacobian(wi, -norm, wo, 1.f / n);

    vec3 const wt   = refractHalfway(wi, norm, wo, n);
    f32 const denom = dot(wi, wt) + n * dot(wo, wt);
    return n * n * gltf::abs(dot(wo, wt)) / (denom * denom);
}

}  // namespace gpu::random
