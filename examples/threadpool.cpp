#include <iostream>

#include <utils/concurrency/Threadpool.hpp>

std::mutex cout_mutex;

int fn1(int x, int y, int z) {
    std::lock_guard<std::mutex> lk{cout_mutex};
    std::cout << "a";
    return x + y + z;
}

double fn2(std::vector<int> v) {
    std::lock_guard<std::mutex> lk{cout_mutex};
    std::cout << "b";
    return static_cast<double>(v.size()) + 0.5;
}

void fn3() {
    std::lock_guard<std::mutex> lk{cout_mutex};
    std::cout << "c";
}

int main() {
    const std::size_t nthreads = std::thread::hardware_concurrency();
    const std::size_t ntasks   = 200;

    std::future<int> first_future;
    std::future<double> second_future;

    gpu::Threadpool threadpool{nthreads};
    for (std::size_t jdx = 0; jdx < ntasks; ++jdx) {
        switch (jdx % 3) {
            case 0: {
                first_future = threadpool.createTask(fn1, 1, 2, 3);
                break;
            }
            case 1: {
                std::vector v{1, 2, 3};
                second_future = threadpool.createTask(fn2, v);
                break;
            }
            case 2: {
                threadpool.createTask(fn3);
                break;
            }
        }
    }
    threadpool.wait();

    {
        std::lock_guard lk{cout_mutex};
        std::cout << std::endl;
        std::cout << first_future.get() << std::endl;
        std::cout << second_future.get() << std::endl;
    }

    return 0;
}