#include <chrono>
#include <iostream>

#include <utils/concurrency/Matrix.hpp>

using matrix = gpu::Matrix<float, gpu::vf32align>;

void print(const matrix &m) {
    for (std::size_t i = 0; i < m.rows(); ++i) {
        for (std::size_t j = 0; j < m.cols(); ++j) {
            std::cout << m(i, j) << ' ';
        }
        std::cout << std::endl;
    }
}

constexpr std::size_t min_size = 16;
constexpr std::size_t max_size = 1024;
constexpr std::size_t step     = 2;

const std::size_t threads = std::thread::hardware_concurrency();
// const std::size_t threads = 2;

template <typename Multiply>
void measure(Multiply multiply, const std::string &type) {
    std::cout << type << std::endl;
    for (std::size_t i = min_size; i <= max_size; i += i / step) {
        std::vector<float> a(i * i);
        for (std::size_t j = 0; j < a.size(); ++j) {
            a[j] = static_cast<float>(j);
        }
        const matrix m{i, i, a};
        const auto begin  = std::chrono::high_resolution_clock::now();
        const matrix prod = multiply(m, m);
        //        print(prod);
        const auto end       = std::chrono::high_resolution_clock::now();
        const matrix correct = multiplyReordered(m, m);
        for (std::size_t k = 0; k < i; ++k) {
            for (std::size_t l = 0; l < i; ++l) {
                if (prod(k, l) != correct(k, l)) {
                    std::cout << "Wrong result for " << type << " of size " << i << std::endl;
                    print(prod);
                    print(correct);
                    k = i;
                    l = i;
                }
            }
        }
        std::cout << i << ' ' << std::chrono::duration<double, std::milli>((end - begin)).count() << std::endl;
    }
    std::cout << std::endl;
}

template <typename Multiply>
void measureThreaded(Multiply multiply, const std::string &type) {
    std::cout << type << std::endl;
    gpu::Threadpool threadpool{threads};
    for (std::size_t i = min_size; i <= max_size; i += i / step) {
        std::vector<float> a(i * i);
        for (std::size_t j = 0; j < a.size(); ++j) {
            a[j] = static_cast<float>(j);
        }
        const matrix m{i, i, a};
        const auto begin  = std::chrono::high_resolution_clock::now();
        const matrix prod = multiply(m, m, threadpool);
        //        print(prod);
        const auto end       = std::chrono::high_resolution_clock::now();
        const matrix correct = multiplyReordered(m, m);
        for (std::size_t k = 0; k < i; ++k) {
            for (std::size_t l = 0; l < i; ++l) {
                if (prod(k, l) != correct(k, l)) {
                    std::cout << "Wrong result for " << type << " of size " << i << std::endl;
                    print(prod);
                    print(correct);
                    k = i;
                    l = i;
                }
            }
        }
        std::cout << i << ' ' << std::chrono::duration<double, std::milli>((end - begin)).count() << std::endl;
    }
    std::cout << std::endl;
}

int main() {
    measure(gpu::multiplyBasic<float, alignof(gpu::vf32)>, "Basic multiplication");
    measureThreaded(gpu::multiplyBasicThreaded<float, alignof(gpu::vf32)>, "Basic multiplication threaded");
    measure(gpu::multiplyReordered<float, alignof(gpu::vf32)>, "Reordered multiplication");
    measureThreaded(gpu::multiplyReorderedThreaded<float, alignof(gpu::vf32)>, "Reordered multiplication threaded ");
    measure(gpu::multiplySIMD, " SIMD multiplication ");
    return 0;
}