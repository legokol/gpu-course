#include <algorithm>
#include <fstream>

#include "utils/ray_tracing/camera.hpp"
#include "utils/ray_tracing/triangle.hpp"
#include "utils/ray_tracing/wfobj.hpp"

#ifndef HEAP
#include "utils/ray_tracing/bvh.hpp"
#define NAME "zero_bvh.ppm"
#else
#include "utils/ray_tracing/heap_bvh.hpp"
#ifdef STACKLESS
#define NAME "zero_stackless_bvh.ppm"
#else
#define NAME "zero_stackful_bvh.ppm"
#endif

#endif

using vec3     = gpu::vec3<float>;
using AABB     = gpu::AABB<float>;
using Camera   = gpu::Camera;
using Ray      = gpu::Ray<float>;
using RayRange = gpu::RayRange<float>;
using Triangle = gpu::Triangle<float>;

int main() {
    std::fstream in(RES_DIR "A6M.obj");
    auto const toBox = [](Triangle const &t) noexcept {
        return AABB{
            min(t.r0, min(t.r1, t.r2)),
            max(t.r0, max(t.r1, t.r2)),
        };
    };

    struct Hit {
        vec3 pos;
        vec3 norm;
    };
#ifndef HEAP
    const auto bvh = gpu::createBVH<float>(gpu::parseOBJ(in), toBox);
#else
    const auto bvh = gpu::createHeapBVH<float>(gpu::parseOBJ(in), toBox);
#endif
    auto const closestHit = [&bvh](Ray const ray, RayRange const range) noexcept -> std::optional<Hit> {
        auto const &[tri, rti] = gpu::intersection(ray, range, bvh);
        if (!tri) {
            return std::nullopt;
        }
        vec3 const r[3] = {tri->r0, tri->r1, tri->r2};
        return hit(rti) ? std::optional<Hit>{{
                              .pos  = interpolate(r, rti.b1, rti.b2),
                              .norm = normalize(cross(r[1] - r[0], r[2] - r[0])),
                          }}
                        : std::nullopt;
    };

    auto const trace = [&](Ray const ray) noexcept -> vec3 {
        vec3 const skyColor   = {0.53f, 0.81f, 0.92f};
        vec3 const lightColor = {1.00f, 0.98f, 0.88f};
        vec3 const lightDir   = gpu::normalize(vec3{3.f, 3.f, -1.f});

        auto const hit = closestHit(ray, RayRange::ZeroToInf());
        if (!hit) return skyColor;

        auto const [pos, norm] = *hit;
        float const NL         = std::max(0.f, dot(norm, lightDir));

        auto const shadowHit = closestHit({pos, lightDir}, {2e-4f, 1.f / 0.f});
        vec3 const albedo    = {0.8f, 0.3f, 0.3f};
        return albedo * (skyColor * 0.1f + lightColor * NL * (shadowHit ? 0.f : 0.7f));
    };

    std::ofstream file(NAME);
    unsigned const width  = 1920u;
    unsigned const height = 1080u;
    file << "P3\n" << width << ' ' << height << "\n255\n";

    Camera const camera = {
        .origin      = {5.f, 3.f, 10.f},
        .at          = {0.f, 0.f, 0.f},
        .up          = {0.f, 1.f, 0.f},
        .fov         = 0.55f,
        .aspectRatio = float(width) / float(height),
    };

    for (unsigned y = 0u; y < height; ++y)
        for (unsigned x = 0u; x < width; ++x) {
            float const u        = -1.f + 2.f * (0.5f + float(x)) / float(width);
            float const v        = 1.f - 2.f * (0.5f + float(y)) / float(height);
            auto const [r, g, b] = trace(camera.castRay(u, v));

            auto const encode = +[](float const f) noexcept {
                float const c = f < 0.0031308f ? f * 12.92f : 1.055f * std::pow(f, 1.f / 2.4f) - 0.055f;
                return unsigned(std::round(255.f * c));
            };
            file << encode(r) << ' ' << encode(g) << ' ' << encode(b) << ' ';
        }
}
