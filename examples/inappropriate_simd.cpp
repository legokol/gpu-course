#include <algorithm>
#include <fstream>
#include <ranges>

#include "utils/ray_tracing/bvh.hpp"
#include "utils/ray_tracing/camera.hpp"
#include "utils/ray_tracing/cylinder.hpp"

using Float = gpu::f32x4;

using vec3f    = gpu::vec3<float>;
using vec3     = gpu::vec3<Float>;
using Cylinder = gpu::Cylinder<Float>;
using Camera   = gpu::Camera;
using Ray      = gpu::Ray<Float>;
using RayRange = gpu::RayRange<Float>;

int main() {
    const Cylinder cylinder[3] = {
        {.r1     = gpu::buildSimdVec({vec3f{1.f, 0.f, 0.f}, {1.f, 1.f, 0.f}, {0.25f, 0.f, 0.f}, {0.f, 0.5f, 0.f}}),
         .r2     = gpu::buildSimdVec({
             vec3f{0.5f, 1.f, 0.f},
             {0.5f, 0.f, 0.f},
             {-0.25f, 1.f, 0.f},
             {0.25f, 1.f, 0.f},
         }),
         .radius = 0.1f},
        {.r1     = gpu::buildSimdVec({vec3f{-0.5f, 0.f, 0.f}, {-0.5f, 0.f, 0.f}, {-1.f, 0.f, 0.f}, {-0.7f, 1.1f, 0.f}}),
         .r2     = gpu::buildSimdVec({vec3f{-0.5f, 1.f, 0.f}, {-1.f, 1.f, 0.f}, {-1.f, 1.f, 0.f}, {-0.8f, 1.1f, 0.f}}),
         .radius = 0.1f},
        {
            .r1     = {0.f, -1000.f, 0.f},
            .r2     = {0.f, 0.f, 0.f},
            .radius = 999.5f,
        }};
    vec3 const albedo[3] = {
        gpu::buildSimdVec({vec3f{0.8f, 0.3f, 0.3f}, {0.8f, 0.3f, 0.3f}, {0.3f, 0.3f, 0.8f}, {0.3f, 0.3f, 0.8f}}),
        gpu::buildSimdVec({vec3f{0.8f, 0.8f, 0.8f}, {0.8f, 0.8f, 0.8f}, {0.8f, 0.8f, 0.8f}, {0.2f, 0.8f, 0.8f}}),
        {0.2f, 0.5f, 0.2f},
    };

    //    struct Hit {
    //        Float::mask_type hit{false};
    //        unsigned cylinderI{0};
    //        Float t{1.f / 0.f};
    //    };
    struct Hit {
        bool hit;
        unsigned cylinderI{0};
        unsigned internalI{0};
        float t{1.f / 0.f};
    };
    auto const closestHit = [&cylinder](Ray const ray, RayRange const range) noexcept -> Hit {
        return std::ranges::fold_left(
            cylinder | std::views::enumerate | std::views::transform([=](auto const &pair) noexcept -> Hit {
                auto const &[i, s]      = pair;
                auto const intersection = gpu::intersection(ray, range, s);
                auto const m1           = gpu::nonempty(intersection);
                auto const m2           = gpu::contains(range, intersection.tMin);
                auto t                  = intersection.tMax;
                where(m2, t)            = intersection.tMin;

                unsigned idx{};
                {
                    float min = 1.f / 0.f;
                    for (unsigned j = 0; j < Float::size(); ++j) {
                        if (m1[j] && t[j] < min) {
                            min = t[j];
                            idx = j;
                        }
                    }
                }
                return {.hit = m1[idx], .cylinderI = unsigned(i), .internalI = idx, .t = t[idx]};
            }),
            Hit{}, [](Hit const &accum, Hit const &hit) noexcept {
                if (!accum.hit) {
                    return hit;
                }
                if (!hit.hit) {
                    return accum;
                }
                return accum.t < hit.t ? accum : hit;
            });
    };

    auto const trace = [&](Ray const &ray) noexcept -> vec3f {
        vec3f const skyColor   = {0.53f, 0.81f, 0.92f};
        vec3f const lightColor = {1.00f, 0.98f, 0.88f};
        vec3 const lightDir    = gpu::normalize(vec3{3.f, 3.f, -1.f});

        auto const hit = closestHit(ray, {0.f, 1.f / 0.f});

        if (!hit.hit) {
            return skyColor;
        }

        auto const &[m, i, j, t] = hit;

        vec3 const pos  = ray.origin + ray.direction * Float{t};
        vec3 const norm = gpu::normal(pos, cylinder[i]);
        float const NL  = gpu::maxf(0.f, dot(norm, lightDir)[j]);

        auto const shadowHit = closestHit({pos, lightDir}, {2e-4f, 1.f / 0.f});
        float const shadow   = shadowHit.hit ? 0.f : 0.7f;
        return vec3f{albedo[i].x[j], albedo[i].y[j], albedo[i].z[j]} * (skyColor * 0.1f + lightColor * NL * shadow);
    };

    std::ofstream file("inappropriate_simd.ppm");
    unsigned const width  = 1920u;
    unsigned const height = 1080u;
    file << "P3\n" << width << ' ' << height << "\n255\n";

    Camera const camera = {
        .origin      = {0.f, 0.8f, -2.f},
        .at          = {0.f, 0.4f, 0.f},
        .up          = {0.f, 1.f, 0.f},
        .fov         = 0.55f,
        .aspectRatio = float(width) / float(height),
    };

    for (unsigned y = 0u; y < height; ++y)
        for (unsigned x = 0u; x < width; ++x) {
            Float const u        = -1.f + 2.f * (0.5f + float(x)) / float(width);
            Float const v        = 1.f - 2.f * (0.5f + float(y)) / float(height);
            auto const [r, g, b] = trace(camera.castRay(u, v));

            auto const encode = +[](float const f) noexcept {
                float const c = f < 0.0031308f ? f * 12.92f : 1.055f * std::pow(f, 1.f / 2.4f) - 0.055f;
                return unsigned(std::round(255.f * c));
            };
            file << encode(r) << ' ' << encode(g) << ' ' << encode(b) << ' ';
        }
}
